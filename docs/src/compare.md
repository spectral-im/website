# What's the difference between Spectral and ...

> For a detailed comparison please visit [here](https://matrix.org/docs/projects/clients-matrix)

## ...Riot Web/Desktop?
* Riot is the most feature-rich Matrix client, supporting integrations, custom widgets, VoIP, etc.
* Spectral does not support E2EE yet. Riot has the best E2EE support in all clients.
* Spectral uses Qt and is a native Matrix client. Riot is a web application.
* Spectral uses far less RAM.
* Riot is designed as an IRC/Slack alternative. Spectral is designed as a WhatsApp/Telegram/Google Hangouts alternative.

## ...Nheko?

* Spectral uses [libQuotient](https://github.com/quotient-im/libQuotient) as SDK, a generic library maintained by [Quotient](https://github.com/quotient-im) for developing Matrix applications; Nheko uses [mtxclient](https://github.com/Nheko-Reborn/mtxclient) written by Nheko's original author(mujx).
* Although both Spectral and Nheko use Qt, Spectral's backend uses Qt, while Nheko's backend uses Boost.
* Spectral does not have E2EE support yet. Nheko has experimental E2EE support.
* Spectral's UI uses QtQuick Controls. Nheko's UI uses Qt Widgets.
* Nheko supports communities.

## ...Fractal?

* Fractal uses Rust and GTK. Spectral uses C++ and Qt. So codes are nowhere close.
* Fractal is GNOME's de facto Matrix client. Spectral has a [KDE incubation plan](https://community.kde.org/Incubator/Projects/Spectral), though it is being worked on.
* Fractal targets GNU/Linux. Spectral targets Windows, macOS and GNU/Linux.
* Spectral is intended to look beautiful on all platforms. Fractal's look fits GNOME environments better.
* Fractal is considered to be a bit more stable than Spectral.

## ...Quaternion?

* Quaternion and Spectral use the same SDK, controller logic, list model with only a few modifications. There are only minor differences between these codes.
* Spectral's UI uses only QtQuick Controls. Quaternion's UI uses Qt Widgets and QtQuick Controls.
* Spectral never has a native look and feel. Quaternion looks native on Qt-based environments.

## ...FluffyChat?

* Spectral uses [libQuotient](https://github.com/quotient-im/libQuotient) as SDK, a generic library maintained by [Quotient](https://github.com/quotient-im) for developing Matrix applications; FluffyChat does not use any Matrix SDK.
* Spectral's backend is written in C++. FluffyChat's backend is written in JavaScript.
* FluffyChat targets Ubuntu Touch. Spectral targets Windows, macOS and GNU/Linux.
