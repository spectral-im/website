# Frequently Asked Questions

Q: What is the Matrix?

A: Nobody can be told what the matrix is, you have to see it for yourself.

Q: What is Spectral?

A: It is a Matrix client.

Q: What system can I run Spectral on?

A: Modern GNU/Linux, Windows and macOS.

Q: Is there a Flatpak package or AppImage?

A: Yes and using them is recommended.

Q: There's no Snap package?

A: Yes. If you want to maintain a Snap package please contact the developer.

Q: The fonts are incorrect/missing/ugly.

A: Install Google Noto Fonts or Roboto, or set custom fonts in settings.

Q: Clicking on links does not open web browser.

A: Install `xdg-desktop-portal-gtk`/`xdg-desktop-portal-kde`, reinstall Spectral and reboot.

Q: DPI issues.

A: Unfortunately Qt Quick's HiDPI is broken for fraction scales such as 125%. This is an upstream bug.

Q: Does Spectral support E2EE?

A: No. 

Q: Will Spectral support E2EE in 2019?

A: [Probably](https://matrix.org/blog/2019/05/07/welcome-to-the-2019-g-so-c-participants).

Q: What SDK does Spectral use?

A: [libQuotient](https://github.com/quotient-im/libQuotient).

Q: Does Spectral use Electron or is Spectral native?

A: Spectral is a native application and does not use Electron.

Q: Is Spectral lightweight?

A: Yes since it does not use Electron.

Q: What can I do to help this project?

A: If you are not familiar with Qt, you can fire bug reports, suggest features and help translation(coming soon). If you are familiar with Qt programming, you can help by fixing bugs or review for unsafe and inefficient code. Moreover, you can buy me some coffee by sending some BTC(1AmNvttxJ6zne8f2GEH8zMAMQuT4cMdnDN) or BCH(bitcoincash:qz8d8ksn6d4uhh2r45cu6ewxy4hvlkq085845hqhds)!
