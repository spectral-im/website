# Compiling from Source

This documentation is for compiling Spectral from command line. If you have Qt Creator, just clone the repository and import.

## Requirements

- Qt 5.12
  - Qt Core 5.12
  - Qt Widgets 5.12
  - Qt Multimedia 5.12
  - Qt Quick 2.12
  - Qt Quick Controls 2.12
  - Qt SVG 5.12
  - Qt Keychain
- CMake
- C++17 compliant compiler (Tested on GCC 9.2/AppleClang 11.0/MSVC 2017)
- [libQuotient](https://github.com/quotient-im/libQuotient)

  libQuotient is already included in the repository. 
  Notice that Spectral usually depends on the HEAD version of libQuotient, 
  so linking to a stable version of libQuotient from the system is probably **not** a good idea.

- cmark
- olm 3


## Compile on GNU/Linux

```bash
git clone --recursive https://gitlab.com/b0/spectral && cd spectral
cmake . -Bbuild
cmake --build build --target install
```

## Compile on FreeBSD

```bash
git clone --recursive https://gitlab.com/b0/spectral && cd spectral
cmake . -Bbuild
cmake --build build --target install
```

## Compile on Windows

Make sure you have MSVC2017 installed.

1. Somehow Install Qt v5.12+

2. Download Spectral source

```bash
git clone --recursive https://gitlab.com/b0/spectral
cd spectral
```

3. Install other dependencies

```bash
git clone https://gitlab.matrix.org/matrix-org/olm.git
cd olm
cmake -LA -G "NMake Makefiles JOM" -H. -Bbuild -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX="install" -DBUILD_SHARED_LIBS=NO
cmake --build build --target install
cd ..
git clone https://github.com/frankosterfeld/qtkeychain.git
cd qtkeychain
cmake -LA -G "NMake Makefiles JOM" -H. -Bbuild -DCMAKE_CXX_FLAGS="/EHsc /W3" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX="install" -DQTKEYCHAIN_STATIC=ON
cmake --build build --target install
cd ..
git clone https://github.com/commonmark/cmark.git
cd cmark
cmake -LA -G "NMake Makefiles JOM" -H. -Bbuild -DCMAKE_CXX_FLAGS="/EHsc /W3" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="install" -DCMARK_SHARED=ON -DCMARK_STATIC=OFF -DCMARK_TESTS=OFF
cmake --build build --target install
cd ..
```

4. Build Spectral

```bash
cmake -LA -G "NMake Makefiles JOM" -H. -Bbuild -DCMAKE_CXX_FLAGS="/EHsc /W3" -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX="install" -DUSE_INTREE_LIBQMC=1 -DQt5Keychain_DIR="qtkeychain/install/lib/cmake/Qt5Keychain" -DOlm_DIR=olm/install/lib/cmake/Olm -DCMARK_LIBRARY=C:/projects/spectral/cmark/install/lib/cmark.lib -DCMARK_INCLUDE_DIR=C:/projects/spectral/cmark/install/include -DDEPLOY_VERBOSITY=%DEPLOY_VERBOSITY%
cmake --build build
```

## Compile on MacOS

1. Update Qt to v5.12+

> In this example Qt 5.13.1 is installed. Make sure to update respective paths if a different version is installed.

```bash
brew install qt5
```

2. Download Spectral source

```bash
git clone --recursive https://gitlab.com/b0/spectral.git && cd spectral
```

3. Install other dependencies

```bash
brew install cmark
git clone https://gitlab.matrix.org/matrix-org/olm.git
pushd olm
cmake . -Bbuild -LA -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=install -DCMAKE_PREFIX_PATH=/usr/local/Cellar/qt/5.13.1/ -DBUILD_SHARED_LIBS=NO
cmake --build build --target install
popd
git clone https://github.com/frankosterfeld/qtkeychain.git
pushd qtkeychain
cmake . -Bbuild -LA -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=install -DCMAKE_PREFIX_PATH=/usr/local/Cellar/qt/5.13.1/ -DQTKEYCHAIN_STATIC=ON
cmake --build build --target install
popd
```

4. Build Spectral

```bash
cmake . -Bbuild -LA -DUSE_INTREE_LIBQMC=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_PREFIX_PATH=/usr/local/Cellar/qt/5.13.1/ -DOlm_DIR=olm/install/lib/cmake/Olm -DQt5Keychain_DIR="qtkeychain/install/lib/cmake/Qt5Keychain"
cmake --build build --target all
```
