# Debug

This documentation is for debugging Spectral from command line. If you have Qt Creator, just run in debug mode.

## Requirements

- Qt 5.12
  - Qt Core 5.12
  - Qt Widgets 5.12
  - Qt Multimedia 5.12
  - Qt Quick 2.12
  - Qt Quick Controls 2.12
- QMake 3.1
- C++14 compliant compiler
- (Optional) libQMatrixClient
- (Optional) Hoedown


## Debug on GNU/Linux

```bash
git clone --recursive https://gitlab.com/b0/spectral
mkdir build && cd build
qmake CONFIG+=debug CONFIG+=qml_debug ../spectral.pro
make -j4
```

Then, run the binary in GDB.

```bash
gdb run spectral
```
