# Unofficial Sources

## Arch Linux

AUR: [spectral-git](https://aur.archlinux.org/packages/spectral-git/)

## Gentoo

Matrix-overlay: [spectral](https://gitlab.com/PureTryOut/matrix-overlay/tree/master/net-im/spectral)
